package com.alfaCentauri.service;


import com.alfaCentauri.domain.Persona;
import com.alfaCentauri.data.PersonaDAO;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.List;

@Stateless
@WebService(endpointInterface = "mx.com.gm.sga.servicio.PersonaServiceWs")
public class PersonaServiceImpl implements IPersonaServiceRemote, IPersonaService, IPersonaServiceWs {

    @Inject
    private PersonaDAO personaDao;

    @Resource
    private SessionContext contexto;

    @Override
    public List<Persona> listarPersonas() {
        return personaDao.listar();
    }

    @Override
    public Persona encontrarPersonaPorId(Persona persona) {
        try {
            return (Persona) personaDao.buscarPorId(persona.getIdPersona());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @param persona Type Persona.
     * @return Return a object Persona.
     **/
    @Override
    public Persona encontrarPersona(Persona persona) {
        try {
            return (Persona) personaDao.buscar(persona);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Persona encontrarPersonaPorEmail(Persona persona) {
        return personaDao.findPersonaByEmail(persona);
    }

    @Override
    public void registrarPersona(Persona persona) {
        try {
            personaDao.insertar(persona);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void modificarPersona(Persona persona) {
        try {
            personaDao.actualizar(persona);
        } catch (Throwable t) {
            contexto.setRollbackOnly();
            t.printStackTrace(System.out);
        }
    }

    @Override
    public void eliminarPersona(Persona persona) {
        try {
            personaDao.eliminar(persona);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
