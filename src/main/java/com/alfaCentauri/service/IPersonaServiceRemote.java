package com.alfaCentauri.service;

import com.alfaCentauri.domain.Persona;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface IPersonaServiceRemote {
    
    public List<Persona> listarPersonas();
    
    public Persona encontrarPersonaPorId(Persona persona);
    
    public Persona encontrarPersonaPorEmail(Persona persona);
    
    public void registrarPersona(Persona persona);
    
    public void modificarPersona(Persona persona);
    
    public void eliminarPersona(Persona persona);
    
}
